#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, shutil #, re, sys, glob, threading
import numpy as np
import pandas as pd
#import addmol.readpos as rp
import addmol.mk_poscar as mp
from addmol import Frac2Cart, Screening, GetCandidateAdSite
f2c = Frac2Cart(); scr = Screening(); getca =GetCandidateAdSite()

threshold = 0.9
N = 10

path = os.getcwd()
vaspfiles = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "vasp" in x])
#dirs = sorted([x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and re.match(r"\d+",x) and len(x) == 2])
lab_in = input("Please input label of atom you want to check, i.e., N1: ")
#N_list = input("Please input the number of atoms you want: ")

dis_tot = []; label_tot = []
for vf in vaspfiles:
    path_pos = path + "/" + vf
    with open(path_pos) as fp: contlines = fp.readlines()
    diss_min, labels_min, uni_vecs_min = getca.get_dissmin(contlines, lab_in, N)
    dis_tot.append(diss_min); label_tot.append(labels_min)

path_info = path + "/info"
if not os.path.exists(path_info): os.mkdir(path_info)
dis_df = pd.DataFrame(dis_tot, index = vaspfiles); dis_df.to_csv(path_info + "/dis_info.dat", sep="\t")
lab_df = pd.DataFrame(label_tot, index = vaspfiles); lab_df.to_csv(path_info + "/lab_info.dat", sep="\t" )
ad_clsts = scr.dendro(dis_df, path_info, lab_in, threshold)

"""[summary]
クラスタリングしたラベルをクラスタごとに並び替えて出力
"""

path_clst = path + "/clst"
if not os.path.exists(path_clst): os.mkdir(path_clst)
for dr in [x for x in os.listdir(path_clst) if os.path.isdir(path_clst + "/" + x)]: shutil.rmtree(path_clst + "/" + dr)

dirs_num = []
for i, ad_clst in enumerate(ad_clsts):
    path_dir = path_clst + "/" + str(ad_clst)
    if not os.path.exists(path_dir): os.mkdir(path_dir)
    shutil.copy(path + "/" + vaspfiles[i], path_dir)
    mp.trans_coorinpos(vaspfiles[i], path_dir, lab_in)

Ltot = ""
dirs_num = [dr for dr in os.listdir(path_clst) if os.path.isdir(path_clst + "/" + dr)]
for dr in dirs_num:
    path_dr = path_clst + "/" + dr
    mp.mk_poss(path_dr)
    selected_pos = sorted([x for x in os.listdir(path_dr) if os.path.isfile(path_dr+"/"+x) and "vasp" in x])[0]
    with open(path_dr + "/" + selected_pos) as fp: poslines = fp.readlines()
    poslines[0] = str(dr) + "\n"
    Ltot += "".join(poslines)

with open(path + "/clusterd_POSCARS", "w") as fw: fw.write(Ltot)
if not os.path.exists(path + "/models"): os.mkdir(path + "/models")
for vf in vaspfiles: shutil.move(vf, path + "/models")

print("\nDone!\n")
# END: Program