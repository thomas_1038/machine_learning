#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, math, shutil #, re, sys, glob, threading
import numpy as np
import pandas as pd
#import addmol.readpos as rp
#import addmol.mk_poscar as mp
import get_density.gd as gd
from addmol import Frac2Cart, Screening, GetCandidateAdSite
from get_density import Gradient_Descent
f2c = Frac2Cart(); scr = Screening(); getca =GetCandidateAdSite(); gd = Gradient_Descent()

threshold = 0.9
N = 10

path = os.getcwd()
vaspfiles = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "vasp" in x])
#lab_in = input("Please input label of atom you want to check, i.e., N1: ")
lab_in = "H1"
#N_list = input("Please input the number of atoms you want: ")

w0  = np.array([-0.770, 0.868, 0.582])
arr = np.random.rand(3); uni_arr = arr/np.linalg.norm(arr)

dis_tot = []; label_tot = []; uni_vecs_tot = []
for vf in vaspfiles:
    path_pos = path + "/" + vf
    with open(path_pos) as fp: contlines = fp.readlines()
    diss_min, labels_min, uni_vecs_min = getca.get_dissmin(contlines, lab_in, N)

    #ba, norm_diff_ba = gd.gradient_descent(uni_vecs_min, diss_min, w0, 0, 70, 1e-8)
    #bb, norm_diff_bb, dot = gd.gradient_descent_const(uni_vecs_min, diss_min, w0, ba, 0, 70, 0.05)
    #print(ba, norm_diff_ba); print(bb, norm_diff_bb, dot)

    b_sum = gd.gd_roop(uni_vecs_min, diss_min, w0[0], w0[1], w0[2], 0.01, 0.005, 70, 1e-12, 0.05, 0.01)
    df = pd.DataFrame(b_sum); df.to_csv(path+"/cc_ab.tsv", sep="\t")
    ba = b_sum[0][0]; bb = b_sum[0][1]; d = (ba + bb)/math.sqrt(2)
    kernel_p, kernel_n = gd.kernel_se(uni_vecs_min, diss_min, d)
    if kernel_p <= kernel_n: bc = np.cross(ba, bb)
    else: bc = np.cross(-1*ba, bb)
    print(ba, bb, bc)
    df_cc = pd.DataFrame([ba, bb, bc]); df_cc.to_csv(path+"/cc_sum.tsv", sep="\t")
    #print(np.dot(ba, bb/np.linalg.norm(bb)))
    #print(np.linalg.norm(ba), np.linalg.norm(bb), np.linalg.norm(bc))
    #print(np.dot(ba, bb), np.dot(bb, bc), np.dot(bc, ba))
    dis_tot.append(diss_min); label_tot.append(labels_min); uni_vecs_tot.append(uni_vecs_min)


#print(uni_vecs_tot)