import math
import numpy as np

class Gradient_Descent:
    def kernel_se(self, E, G, d):
        E = np.array(E); G = np.array(G)
        kernel_posi = [G[i] * np.exp(np.dot(E[i], d)) for i in range(len(E)) if np.dot(E[i], d) >= 0]
        kernel_nega = [G[i] * np.exp(np.dot(E[i], d)) for i in range(len(E)) if np.dot(E[i], d) < 0]

        return kernel_posi, kernel_nega

    def gradient_ec(self, E, G, w):
        I = np.eye(3)
        wr = w.reshape(-1, 1); wwT = wr*w
        wE = np.dot(w, E.T)

        #grad = sum(np.array([(-1 * G[i] * np.arccos(wE[i])/math.sqrt(1.0 - wE[i]**2)) * E[i] for i in range(len(wE))]))
        grad = sum(np.array([-1 * G[i] * np.exp(-1 * wE[i]) * E[i] for i in range(len(wE))]))
        grad_ver = np.dot((I - wwT), grad)

        return grad, grad_ver

    def gradient_sa(self, E, G, w):
        I = np.eye(3)
        wr = w.reshape(-1, 1); wwT = wr*w
        wE = np.dot(w, E.T)

        grad = sum(np.array([(-1 * G[i] * np.arccos(wE[i])/math.sqrt(1.0 - wE[i]**2)) * E[i] for i in range(len(wE))]))
        #grad = sum(np.array([-1 * G[i] * np.exp(-1 * wE[i]) * E[i] for i in range(len(wE))]))
        grad_ver = np.dot((I - wwT), grad)

        return grad, grad_ver

    def rotation(self, w, theta):
        cos = np.cos(theta/180.0*np.pi)
        sin = np.sin(theta/180.0*np.pi)

        Rot = np.array([[cos, -1*sin, 0], [sin, cos, 0], [0, 0, 1]])
        w_rot = np.dot(Rot, np.array(w))

        return w_rot

    def gradient_descent(self, E, G, w0, typ, step_num=70, th=1e-12):
        w = w0
        for i in range(step_num):
            #print("i: " + str(i))
            if typ == "ec": grad_v = self.gradient_ec(np.array(E), np.array(G), w)[1]
            else: grad_v = self.gradient_sa(np.array(E), np.array(G), w)[1]

            if i == 0: alpha = 0.01
            else:
                diff_grad_v = grad_v - grad_v_pre
                if typ == "ec": alpha = abs(np.dot(diff_w, diff_grad_v)/np.linalg.norm(diff_grad_v)**2)
                else: alpha = np.dot(diff_w, diff_grad_v)/np.linalg.norm(diff_grad_v)**2

            # Save
            w_pre = w; grad_v_pre = grad_v
            # Update
            w = w - alpha * grad_v

            diff_w = w - w_pre
            if np.linalg.norm(diff_w) < th: break

        return w, np.linalg.norm(diff_w)

    def gradient_descent_const(self, E, G, w0, ba, typ, step_num=70, th1=0.05, th2=1e-2):
        w = w0
        #print(np.dot(w, ba))

        for i in range(step_num):
            #print("ib: " + str(i))
            if typ == "ec": grad_v = self.gradient_ec(np.array(E), np.array(G), w)[1]
            else: grad_v = self.gradient_sa(np.array(E), np.array(G), w)[1]
            #print(np.dot(w, grad_v))

            if i == 0: alpha = 0.01
            else:
                diff_grad_v = grad_v - grad_v_pre
                if typ == "ec": alpha = abs(np.dot(diff_w, diff_grad_v)/np.linalg.norm(diff_grad_v)**2)
                else: alpha = np.dot(diff_w, diff_grad_v)/np.linalg.norm(diff_grad_v)**2

            # Save
            w_pre = w; grad_v_pre = grad_v
            # Update
            w = w - alpha * grad_v

            diff_w = w - w_pre
            #print(np.linalg.norm(diff_w), np.dot(ba,w), w)
            if (np.linalg.norm(diff_w) < th1) and (abs(np.dot(ba, w)) < th2): break

        return w/np.linalg.norm(w), np.linalg.norm(diff_w), np.dot(ba, w)

    def gd_roop(self, E, G, a1, a2, a3, rg, step, itr=70, th_a=1e-12, th_b1=0.05, th_b2=0.01):
        b_sum = []
        for i1 in np.arange(a1-rg, a1+rg, step):
            for i2 in np.arange(a2-rg, a2+rg, step):
                for i3 in np.arange(a3-rg, a3+rg, step):
                    w0 = np.array([i1, i2, i3])
                    print(w0)
                    ba, norm_diff_ba = self.gradient_descent(E, G, w0, "ec", itr, th_a)
                    bb, norm_diff_bb, dot = self.gradient_descent_const(E, G, w0, ba, "ec", itr, th_b1, th_b2)

                    if norm_diff_bb < th_b1 and dot < th_b2: b_sum.append([ba, bb, w0, norm_diff_ba, norm_diff_bb, dot])
        return b_sum