#%%
#import os, sys, shutil, threading, glob, re
import numpy as np
import matplotlib.pyplot as plt

def numerical_gradient(f, x, h):
    grad = np.zeros_like(x)

    for i in range(len(x)):
        tmp_val = x[i]

        #x[0]にx[0]+hを代入
        x[i] = tmp_val + h
        fxh1 = f(x) #; print(fxh1)

        #x[0]にx[0]-hを代入
        x[i] = tmp_val - h
        fxh2 = f(x) #; print(fxh2)

        grad[i] = (fxh1 - fxh2)/(2*h) # ; print(grad[i])
        x[i] = tmp_val

    return grad

def gradient_descent(f, init_x, lr=0.01, step_num=100, th=1e-8):
    x = init_x
    for i in range(step_num):
        f_pre = f(x)
        grad = numerical_gradient(f, x, 1e-4)
        x -= lr * grad
        if abs(f(x) -f_pre) < th: break
    return x, i

#%%
def function2(x):
    return x[0]**4 - x[0]**3

gradient_descent(function2, init_x=np.array([0.1]), lr=0.1, step_num=1000, th=1e-15)
